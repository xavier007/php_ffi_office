# php_ffi_office

#### 介绍

php通过FFI读写Excel   
PHP7.4提供FFI扩展，可以方便的调用C接口，此项目通过golang开发共享库，PHP通过FFI调用，从而读取xlsx    
目前，项目提供如下功能

1. 读取sheet数量
2. 遍历读取数据
3. 通过指定位置读取数据
4. 允许写入Excel
5. 获取单元格格式
6. 修改单元格格式
7. 获取单元格样式
8. 修改单元格样式
9. 设置单元格公式 10.设置单元格公式

计划增加功能

1. 读取Excel单元格格式
2. 写入单元格
3. 插入图片
4. 修改单元格格式
5. 插入图表
6. 增加word和PPT

#### demo

```php
// 加载头文件
$util=FFI::load('ffi.h');
// 打开文件
$handle = $util->OpenFile("1.xlsx");
// 获取sheet数量
var_dump($util->GetSheetCount($handle));
// 获取row对象
$row = $util->GetRow($handle,"Sheet1");

$rows=[];
// 获取下一行 NextRow 存在则返回1 否则0
while($util->NextRow($row)){
    // 获取行数据
    $x=$util->GetRowData($row);
    $_row =[];
    // Length 为单行col数量 
    for($i=0;$i<$x->Length;$i++){
        // 为当前字符串指针
        $a = $x->Data;
        // 读取字符串
        $_row[]=FFI::string($a[0]);
        // 下一个指针
        $x->Data++;
    }
    $rows[]= $_row;
}
var_dump($rows);
// 获取行数量
var_dump($util->GetRowCount($handle,"Sheet1"));
// 通过指定位置获取数据
var_dump(FFI::string($util->GetCellValue($handle,"Sheet1","E1")));
// 关闭文件
$util->Close($handle);
```

golang部分，采用开源Excel处理库

```
github.com/xuri/excelize/v2
```
