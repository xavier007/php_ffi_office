package main

import "C"
import (
	"fmt"
	"github.com/xuri/excelize/v2"
	"unsafe"
)

// SetActiveSheet 设置默认sheet
//export SetActiveSheet
func SetActiveSheet(f unsafe.Pointer, index C.int) {
	file := (*excelize.File)(f)
	file.SetActiveSheet(int(index))
}

// SetCellValueInt 设置指定位置值
//export SetCellValueInt
func SetCellValueInt(f unsafe.Pointer, sn *C.char, axis *C.char, val C.int) {
	sheeteName := C.GoString(sn)
	_axis := C.GoString(axis)
	file := (*excelize.File)(f)
	err := file.SetCellInt(sheeteName, _axis, int(val))
	if err != nil {
		fmt.Println(err)
	}
}

// SetCellValueDouble 设置指定位置值
//export SetCellValueDouble
func SetCellValueDouble(f unsafe.Pointer, sn *C.char, axis *C.char, val C.double) {
	sheeteName := C.GoString(sn)
	_axis := C.GoString(axis)
	file := (*excelize.File)(f)
	err := file.SetCellValue(sheeteName, _axis, float64(val))
	if err != nil {
		fmt.Println(err)
	}
}

// SetCellValueString 设置指定位置值
//export SetCellValueString
func SetCellValueString(f unsafe.Pointer, sn *C.char, axis *C.char, val *C.char) {
	sheeteName := C.GoString(sn)
	_axis := C.GoString(axis)
	file := (*excelize.File)(f)
	err := file.SetCellStr(sheeteName, _axis, C.GoString(val))
	if err != nil {
		fmt.Println(err)
	}
}

// NewSheet 新建sheet  返回sheet编号
//export NewSheet
func NewSheet(f unsafe.Pointer, sn *C.char) C.int {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	index := file.NewSheet(sheeteName)
	return C.int(index)
}

// SetRowHeight 设置行高
//export SetRowHeight
func SetRowHeight(f unsafe.Pointer, sn *C.char, row C.int, height C.double) {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	err := file.SetRowHeight(sheeteName, int(row), float64(height))
	if err != nil {
		fmt.Println(err)
	}
}

// SetRowVisible 设置行显示
//export SetRowVisible
func SetRowVisible(f unsafe.Pointer, sn *C.char, row C.int, visible C.int) {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	_visible := false
	if visible >= 0 {
		_visible = true
	}
	err := file.SetRowVisible(sheeteName, int(row), _visible)
	if err != nil {
		fmt.Println(err)
	}
}

// RemoveRow 删除行
//export RemoveRow
func RemoveRow(f unsafe.Pointer, sn *C.char, row C.int) {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	err := file.RemoveRow(sheeteName, int(row))
	if err != nil {
		fmt.Println(err)
	}
}

// SetRowStyle 设置行类型
//export SetRowStyle
func SetRowStyle(f unsafe.Pointer, sn *C.char, start, end, styleID C.int) {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	err := file.SetRowStyle(sheeteName, int(start), int(end), int(styleID))
	if err != nil {
		fmt.Println(err)
	}
}

// SetColStyle 设置列类型
//export SetColStyle
func SetColStyle(f unsafe.Pointer, sn *C.char, col *C.char, styleID C.int) {
	sheeteName := C.GoString(sn)
	colName := C.GoString(col)
	file := (*excelize.File)(f)
	err := file.SetColStyle(sheeteName, colName, int(styleID))
	if err != nil {
		fmt.Println(err)
	}
}

// SetCellFormula 设置指定位置公式
//export SetCellFormula
func SetCellFormula(f unsafe.Pointer, sn *C.char, aix *C.char, c *C.char) {
	sheeteName := C.GoString(sn)
	axis := C.GoString(aix)
	content := C.GoString(c)
	file := (*excelize.File)(f)
	err := file.SetCellDefault(sheeteName, axis, content)
	if err != nil {
		fmt.Println(err)
	}
}

// MergeCell 合并单元格
//export MergeCell
func MergeCell(f unsafe.Pointer, sn *C.char, aix1 *C.char, aix2 *C.char) {
	sheeteName := C.GoString(sn)
	axis1 := C.GoString(aix1)
	axis2 := C.GoString(aix2)
	file := (*excelize.File)(f)
	err := file.MergeCell(sheeteName, axis1, axis2)
	if err != nil {
		fmt.Println(err)
	}
}

// UnmergeCell 取消合并单元格
//export UnmergeCell
func UnmergeCell(f unsafe.Pointer, sn *C.char, aix1 *C.char, aix2 *C.char) {
	sheeteName := C.GoString(sn)
	axis1 := C.GoString(aix1)
	axis2 := C.GoString(aix2)
	file := (*excelize.File)(f)
	err := file.UnmergeCell(sheeteName, axis1, axis2)
	if err != nil {
		fmt.Println(err)
	}
}
