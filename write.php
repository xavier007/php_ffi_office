<?php
$util=FFI::load('ffi.h');
// 新建文件
$handle = $util->NewFile();
$index = $util->NewSheet($handle,"sheet2");
$util->SetActiveSheet($handle,$index );
$util->SetCellValueInt($handle,"sheet2","A1",100);
$util->SetCellValueDouble($handle,"sheet2","B1",20.3);
$util->SetCellFormula($handle,"sheet2","C1","=A1+B1");
// 保存文件 不可以存在同名文件
$util->SaveAs($handle,"22.xlsx");
unset($util);