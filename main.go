package main

import "C"
import (
	"fmt"
	"github.com/xuri/excelize/v2"
	"unsafe"
)

//go build -o libutil.so -buildmode=c-shared main.go
// export GODEBUG=cgocheck=0

// NewFile  新建文件
//export NewFile
func NewFile() unsafe.Pointer {
	f := excelize.NewFile()
	return unsafe.Pointer(f)
}

// Save  保存
//export Save
func Save(f unsafe.Pointer, fn *C.char) {
	file := (*excelize.File)(f)
	err := file.Save()
	if err != nil {
		fmt.Println(err)
	}
}

// SaveAs  另存为
//export SaveAs
func SaveAs(f unsafe.Pointer, fn *C.char) {
	fileName := C.GoString(fn)
	file := (*excelize.File)(f)
	err := file.SaveAs(fileName)
	if err != nil {
		fmt.Println(err)
	}
}

// OpenFile  打开文件
//export OpenFile
func OpenFile(fn *C.char) unsafe.Pointer {
	fileName := C.GoString(fn)
	f, err := excelize.OpenFile(fileName)
	if err != nil {
		fmt.Println(err)
	}
	return unsafe.Pointer(f)
}

// NextRow 是否存在下一行  1存在 0 不存在
//export NextRow
func NextRow(r unsafe.Pointer) C.int {
	row := (*excelize.Rows)(r)
	if row.Next() {
		return C.int(1)
	}
	return C.int(0)
}

// GetRow 获取row对象
//export GetRow
func GetRow(f unsafe.Pointer, sn *C.char) unsafe.Pointer {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	rows, err := file.Rows(sheeteName)
	if err != nil {
		fmt.Println(err)
	}
	return unsafe.Pointer(rows)
}

// GetRowData 获取行数据
//export GetRowData
func GetRowData(r unsafe.Pointer) unsafe.Pointer {
	row := (*excelize.Rows)(r)
	Columns, ColumnsErr := row.Columns()
	if ColumnsErr != nil {
		fmt.Println(ColumnsErr)
	}
	size := len(Columns)
	type CCharPtr *C.char
	p := C.malloc(C.size_t(size) * C.size_t(unsafe.Sizeof(C.int(0))))
	chars := (*[1<<30 - 1]CCharPtr)(p)[:size:size]
	for index, Column := range Columns {
		chars[index] = C.CString(Column)
	}
	var data RowData
	data.Length = size
	if len(chars) == 0 {
		return unsafe.Pointer(&data)
	}
	data.Data = unsafe.Pointer(&chars[0])
	data.Row = unsafe.Pointer(row)
	return unsafe.Pointer(&data)
}

type RowData struct {
	Length int
	Data   unsafe.Pointer
	Row    unsafe.Pointer
}

// Close 关闭
//export Close
func Close(f unsafe.Pointer) {
	file := (*excelize.File)(f)
	file.Close()
}

func main() {

}
