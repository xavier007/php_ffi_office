package main

import "C"
import (
	"fmt"
	"github.com/xuri/excelize/v2"
	"unsafe"
)

//export GetRowCount
func GetRowCount(f unsafe.Pointer, sn *C.char) C.int {
	sheeteName := C.GoString(sn)
	file := (*excelize.File)(f)
	rows, err := file.GetRows(sheeteName)
	if err != nil {
		fmt.Println(err)
	}

	return C.int(len(rows))
}

// GetSheetCount 获取sheet数量
//export GetSheetCount
func GetSheetCount(f unsafe.Pointer) C.int {
	file := (*excelize.File)(f)
	return C.int(file.SheetCount)
}

// GetCellType 获取cell类型
//export GetCellType
func GetCellType(f unsafe.Pointer, sn *C.char, aix *C.char) C.int {
	sheeteName := C.GoString(sn)
	axis := C.GoString(aix)
	file := (*excelize.File)(f)
	cellType, err := file.GetCellType(sheeteName, axis)
	if err != nil {
		fmt.Println(err)
	}
	return C.int(cellType)
}

// GetCellValue 读取指定位置数据
//export GetCellValue
func GetCellValue(f unsafe.Pointer, sn *C.char, axis *C.char) *C.char {
	sheeteName := C.GoString(sn)
	_axis := C.GoString(axis)
	file := (*excelize.File)(f)
	val, err := file.GetCellValue(sheeteName, _axis)
	if err != nil {
		fmt.Println(err)
	}

	return C.CString(val)
}

// GetCellStyle 获取cell样式
//export GetCellStyle
func GetCellStyle(f unsafe.Pointer, sn *C.char, aix *C.char) C.int {
	sheeteName := C.GoString(sn)
	axis := C.GoString(aix)
	file := (*excelize.File)(f)
	cellType, err := file.GetCellStyle(sheeteName, axis)
	if err != nil {
		fmt.Println(err)
	}
	return C.int(cellType)
}

// GetActiveSheetIndex 获取当前sheet编号
//export GetActiveSheetIndex
func GetActiveSheetIndex(f unsafe.Pointer, sn *C.char, aix *C.char) C.int {
	file := (*excelize.File)(f)
	index := file.GetActiveSheetIndex()
	return C.int(index)
}

// GetCellFormula 获指定位置的公式
//export GetCellFormula
func GetCellFormula(f unsafe.Pointer, sn *C.char, aix *C.char) *C.char {
	sheeteName := C.GoString(sn)
	axis := C.GoString(aix)
	file := (*excelize.File)(f)
	content, err := file.GetCellFormula(sheeteName, axis)
	if err != nil {
		fmt.Println(err)
	}
	return C.CString(content)
}
