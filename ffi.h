#define FFI_LIB "./libutil.so"

struct  datas  {
    int Length;
    char** Data;
};

// NewFile  新建文件
void* NewFile();

// Save  保存
void Save(void* f, char* fn);

// SaveAs  另存为
void SaveAs(void* f, char* fn);

// OpenFile  打开文件
void* OpenFile(char* fn);

// NextRow 是否存在下一行  1存在 0 不存在
int NextRow(void* r);

// GetRow 获取row对象
void* GetRow(void* f, char* sn);

// GetRowData 获取行数据
struct  datas* GetRowData(void* r);

// Close 关闭
void Close(void* f);

// SetActiveSheet 设置默认sheet
void SetActiveSheet(void* f, int index);

// SetCellValueInt 设置指定位置值
void SetCellValueInt(void* f, char* sn, char* axis, int val);

// SetCellValueDouble 设置指定位置值
void SetCellValueDouble(void* f, char* sn, char* axis, double val);

// SetCellValueString 设置指定位置值
void SetCellValueString(void* f, char* sn, char* axis, char* val);

// NewSheet 新建sheet  返回sheet编号
int NewSheet(void* f, char* sn);

// SetRowHeight 设置行高
void SetRowHeight(void* f, char* sn, int row, double height);

// SetRowVisible 设置行显示
void SetRowVisible(void* f, char* sn, int row, int visible);

// RemoveRow 删除行
void RemoveRow(void* f, char* sn, int row);

// SetRowStyle 设置行类型
void SetRowStyle(void* f, char* sn, int start, int end, int styleID);

// SetColStyle 设置列类型
void SetColStyle(void* f, char* sn, char* col, int styleID);

// SetCellFormula 设置指定位置公式
void SetCellFormula(void* f, char* sn, char* aix, char* c);

// MergeCell 合并单元格
void MergeCell(void* f, char* sn, char* aix1, char* aix2);

// UnmergeCell 取消合并单元格
void UnmergeCell(void* f, char* sn, char* aix1, char* aix2);
int GetRowCount(void* f, char* sn);

// GetSheetCount 获取sheet数量
int GetSheetCount(void* f);

// GetCellType 获取cell类型
int GetCellType(void* f, char* sn, char* aix);

// GetCellValue 读取指定位置数据
char* GetCellValue(void* f, char* sn, char* axis);

// GetCellStyle 获取cell样式
int GetCellStyle(void* f, char* sn, char* aix);

// GetActiveSheetIndex 获取当前sheet编号
int GetActiveSheetIndex(void* f, char* sn, char* aix);

// GetCellFormula 获指定位置的公式
char* GetCellFormula(void* f, char* sn, char* aix);